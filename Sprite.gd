extends Sprite
class_name Gun
export(PackedScene) var bullet
export(String) var level_name
var shooting = true
var wait_time = 0.5
var damage = 1
var target
var ammo = 20
var b


func _input(event):
	if event is InputEventMouseMotion:
		target = event.position
		look_at(target)
func _process(delta):
	if Input.is_action_pressed("fire"):
		_shoot(bullet)
func _shoot(bul):
	if shooting:
		b = bul.instance()
		b.position = $muzzle.global_position
		b.rotation = rotation
		b.damage = damage
		b = change_bullet(b)
		
		get_node("/root/"+level_name).add_child(b)
		shooting = false
		yield(get_tree().create_timer(wait_time),"timeout")
		shooting = true
		
	

func change_bullet(b):
	return b
	