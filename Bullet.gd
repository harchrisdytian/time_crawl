extends Area2D

class_name Bullet
var speed = 10
var player_hit = true
var damage = 1

func move():
	position += Vector2(1,0).rotated(rotation) * speed

func extra_things():
	pass

func _process(delta):
	move()
	extra_things()

func _on_Bullet_body_entered(body):
	if player_hit:
		if !body.name == "Player":
			if body.has_method("take_damage"):
				body.take_damage()
				_die()
	else:
			if body.has_method("take_damage"):
				body.take_damage()
				_die()
	if body is TileMap:
		_die()

func death_event():
	pass
func _die():
	death_event()
	queue_free()
	