extends Bullet

export (PackedScene) var bullet
var count = 5
var step = 2 * PI
var radius =40
func death_event():
	var angle = 0
	for i in range(count):
		var direction = Vector2(cos(angle),sin(angle))
		var pos = direction * radius
		
		var b =bullet.instance()
		b.position = global_position * pos
		b.speed = speed
		get_parent().get_parent().add_child(b)
		