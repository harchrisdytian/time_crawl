extends Control

var ItemPanel = preload("res://Item.tscn")
var items = [preload("res://Gun.tscn"),preload("res://Rifle.tscn"),preload("res://FlameThrower.tscn"),preload("res://Gun.tscn")] setget item_changed
signal items_change

func update_display():
	for child in $ScrollContainer/Items.get_children():
		child.queue_free()
	
	for item in items:
		add_item(item)
		
func add_item(item):
	var i = ItemPanel.instance()
	var it = item.instance()
	i.set_item(it)
	i.item = item
	$ScrollContainer/Items.add_child(i)
	it.queue_free()

func _ready():
	update_display()
	remove_item([preload("res://Gun.tscn")])

func get_selected():
	var selected = []
	for i in $ScrollContainer/Items.get_children():
		if i.selected:
			selected.append(i.item)
	return selected
	
	
func item_changed(value):
	items = value
	update_display()
	emit_signal("items_change",items)

func remove_item(v):
	for i in v:
		var j = [] #set a blank array
		items.remove(items.find(i))
		j = items 
		self.items = j
	