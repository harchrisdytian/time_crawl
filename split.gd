extends Control

var left_items  = [] setget set_left_item
var right_items = [] setget set_right_items
signal left
signal right
func _ready():
	pass
	
func set_left_item(value):
	left_items = value
	$HSplitContainer/left_inv.items = value
	emit_signal("right",value)
	print(value)

func set_right_items(value):
	right_items = value
	$HSplitContainer/right_inv.items = value
	emit_signal("left",value)

func _on_left_pressed():
	var shifted = $HSplitContainer/left_inv.get_selected()
#	print(shifted)
	var k = right_items + shifted
	self.right_items = right_items + shifted
	$HSplitContainer/left_inv.remove_item(shifted)


func _on_right_pressed():
	var shifted = $HSplitContainer/right_inv.get_selected()
	var k = left_items + shifted
	self.left_items = k
	$HSplitContainer/right_inv.remove_item(shifted)