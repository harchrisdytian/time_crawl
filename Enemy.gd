extends KinematicBody2D

var health = 5
signal take_damage
signal take_cover
var Bullet = preload("res://Bullet.tscn")
var target
var player setget set_player

var speed = 100
var velocity = Vector2()
var bullet_speed = 10
var cover_position = []

var path = PoolVector2Array()
var cover 
var shoot

var goals = []
class Goal:
	var cost
	var methoid = ""
	var function 
	var Self
	func _init(c,s,funct):
		cost = c
		Self = s
		function = funct
		
	func set_cost(c):
		cost = c

	func get_cost():
		return cost
	func run_methoid():
		pass
	func set_func(Function,s = Self):
		Self = s
		function = function
	func call_func():
		cost = cost -1
		Self.call(function)



func set_player(value):
	player = value
	target = player.position


func take_damage():
	health -= 1
	if health <= 0:
		queue_free()


func _on_Enemy_take_cover():
	$Player.global_transfrom()
	

	if player != null:
		target = position - player.position 

func _ready():
	cover = Goal.new(10,self,"find_cover")
	shoot = Goal.new(10,self,"shoot_gun")
	goals = [cover,shoot]
	
func movement():
	velocity = Vector2()
	if path.size() > 0:
		velocity = ( path[0] - position).round().normalized() * speed
#		print(position.distance_squared_to(path[0]))
		
		if position.distance_squared_to(path[0]) <= 100:
			path.remove(0)
	move_and_slide(velocity)
	if not player == null:
		look_at(target)
	pass

func _on_Timer_timeout():
	if target != null:
		var b = Bullet.instance()
		b.global_rotation = (target - global_position).angle() + deg2rad(180)
		b.speed = bullet_speed
		b.global_position = $Muzzle.global_position
		get_parent().add_child(b)

func find_cover():
		cover_position = yield(get_parent().search(),"completed")
		if !cover_position.size() == 0:
			path = get_parent().find_cover(global_position,cover_position[0])
		$Timer.stop()
		print('k')

func shoot_gun():
	if $Timer.is_stopped():
		$Timer.start()

func _process(delta):
	movement()

func caculate_goals():
	print(cover.get_cost(),shoot.get_cost())
	if cover.get_cost() > shoot.get_cost():
		cover.call_func()
	else:
		shoot.call_func()
	
