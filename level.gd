extends Node2D

var hiding_spot=Vector2()
var cover_spots = []
onready var current_chest = $chest


func _process(delta):
	for child in get_children():
		if child.is_in_group("enemies"):
			child.player = $Player
	if Input.is_action_just_pressed("ui_end"):
		pause()
	if Input.is_action_just_pressed("open"):
		pause()
		close_inventory()
func _ready():
	$CanvasLayer/inventory.left_items = $Player.inventory

func pause():
	if get_tree().paused:
		get_tree().paused = false
	else:
		get_tree().paused = true

func search(enemy = null):
	$Search.player = $Player.position
	cover_spots = [] 
	for tiles in $AI.get_used_cells():
		hiding_spot = $AI.map_to_world(tiles) + ($AI.cell_size / 2)
		$Search.position = hiding_spot
		yield(get_tree().create_timer(0.03),"timeout")
		if $Search.is_coverd():
			print($Search.is_coverd())
			cover_spots.append($Search.position)
		yield(get_tree().create_timer(0.04),"timeout")
	return cover_spots
	
	

func find_cover(start,end):
	return $Navigation2D.get_simple_path(start,end,false)

func open_invenotory():
	pause()
	$CanvasLayer/inventory.show()
	$CanvasLayer/inventory.left_items = $Player.items
func close_inventory():
	$CanvasLayer/inventory.hide()
func _on_inventory_left(p):
	$Player.inventory = p


func _on_chest_inventory(inv,Self):
	current_chest = Self
	$CanvasLayer/inventory.right_items = inv
	open_invenotory()

func _on_inventory_right(inv):
	current_chest.inventory = inv
