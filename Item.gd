extends ColorRect

export (Color) var  highlight
onready var normal_color = self.color
var is_hovered = false
var selected = false
var item

func set_item(item):
	$Holder/Icon.texture = item.texture
	$Name.text = item.name
	if item is Gun:
		$Ammo.text = "ammo: " + str(item.ammo)
	else:
		$ammo.hide()


func _process(delta):
	if Input.is_action_just_pressed("fire") and is_hovered:
		if !select():
			deselect()


func select():
	if !selected:
		$Select.show()
		selected = true
		return true
	else:
		 return false

func deselect():
	if selected:
		$Select.hide()
		selected =false
		return true
	else: 
		return

func _on_Item_mouse_entered():
	color = highlight
	is_hovered = true

func _on_Item_mouse_exited():
	color = normal_color
	is_hovered = false



