extends KinematicBody2D

enum states {IDLE,WALKING,RUNNING,ROLL}
var items = []

const RUN_SPEED = 300
const ROLL = 600
var accel = 0
var state = states.WALKING
var old_direction = Vector2()
var direction = Vector2()
var velocity = Vector2()

var inventory  setget set_inventory
var inventory_images = []
var current_item = 0
var is_hidden = false

func change_state(new_state):
	state = new_state
	match new_state:
		states.WALKING:
			$RunTimer.start()
		states.RUNNING:
			pass
		states.ROLL:
			$CollisionShape2D.set_deferred("disabled", false)
			$RollTimer.start()
		states.IDLE:
			accel= 0

func _process(delta):
	if state == states.IDLE:
		if Input.is_action_pressed("up"):
			change_state(states.WALKING)
		if Input.is_action_pressed("down"):
			change_state(states.WALKING)
		if Input.is_action_pressed("left"):
			change_state(states.WALKING)
		if Input.is_action_pressed("right"):
			change_state(states.WALKING)
	
	invetory_switch()
	#walking
	if state in [states.WALKING ,states.RUNNING]:
		if Input.is_action_pressed("up"):
			direction.y = -1
		elif Input.is_action_pressed("down"):
			direction.y = 1
		else:
			direction.y = 0
		if Input.is_action_pressed("left"):
			direction.x = 1
		elif Input.is_action_pressed("right"):
			direction.x = -1
		else:
			direction.x = 0
		if Input.is_action_just_pressed("roll"):
			change_state(states.ROLL)
		direction = direction.normalized()
		
		
		accel = lerp(accel, RUN_SPEED, 5 * delta)
		
		velocity = direction * accel
		
		velocity = move_and_slide(velocity)
		
		if direction == Vector2():
			change_state(states.IDLE)
			
		if direction != Vector2():
			old_direction = direction
		direction = Vector2()
	#rolling
	if state == states.ROLL:
		velocity = old_direction * ROLL
		velocity = move_and_slide(velocity)
#	print(velocity)	
	

func _on_RollTimer_timeout():
	change_state(states.IDLE)
	$CollisionShape2D.set_deferred("disabled", false)

func _on_RunTimer_timeout():
	change_state(states.RUNNING)

func take_damage():
	print("taking damage")

##############################################################################################################
#                                         Inventory control                                                  #
##############################################################################################################


func _ready():
	self.inventory = [preload("res://Gun.tscn")]
	$Container/Inventory.items = inventory

func invetory_switch():

	if Input.is_action_just_pressed("switch_left"):
		$Container/Item.texture = switch("left",current_item,inventory)
	if Input.is_action_pressed("switch_right"):
		$Container/Item.texture = switch("left",current_item,inventory)
		
	if Input.is_action_just_pressed("open"):
		if is_hidden:
			$Container/Inventory.show()
			is_hidden = false
		else:
			is_hidden = true
			$Container/Inventory.hide()

func set_inventory(value):
	inventory = value
	for items in inventory:
		var i = items.instance()
		inventory_images.append(i.texture)
		i.queue_free()

func switch(dir,num,array):
	#lets the items cycle between list in array
	
	if dir == "left":#if left then you shift it to the left
		num += -1
		if num < 0:
			num = array.size() - 1
	elif dir == "right":
		num +=1
		if num > (array.size() -1):
			num = 0
	#
	return array[num]